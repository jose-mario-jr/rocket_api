use postgres::{Client, Error, NoTls};

mod secrets;

#[allow(dead_code)]
static STRING_CONEXAO: &str = secrets::URL;

#[allow(dead_code)]
pub static SCHEMA: &str = "CREATE TABLE people (
  id              SERIAL PRIMARY KEY,
  name            VARCHAR NOT NULL,
  age             INTEGER,
  date            VARCHAR
)";

#[allow(dead_code)]
pub static DROP_ALL: &str = "DROP TABLE people";

#[allow(dead_code)]
pub static INSERT_PERSON: &str = "INSERT INTO people (name, age, date) VALUES ($1, $2, $3)";

#[allow(dead_code)]
pub static GET_PEOPLE: &str = "SELECT * FROM people";

#[allow(dead_code)]
pub static GET_PERSON: &str = "SELECT * FROM people WHERE id = $1";

#[allow(dead_code)]
pub fn get_conn() -> Result<Client, Error> {
  Client::connect(STRING_CONEXAO, NoTls)
}

#[allow(dead_code)]
pub fn get_configs() -> secrets::Configs {
  secrets::PG_CONFIGS
}
