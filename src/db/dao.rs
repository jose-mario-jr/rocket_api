#[path = "conexao.rs"]
mod conexao;

use rocket_contrib::json::JsonValue;

#[allow(dead_code)]
pub fn execute(query: &'static str) -> JsonValue {
  match conexao::get_conn() {
    Ok(mut conn) => match conn.execute(query, &[]) {
      Ok(r) => json!({ "Sucess!": format!("{:?}", r) }),
      Err(e) => json!({ "Error:  ": e.to_string() }),
    },
    Err(e) => json!(format!("Erro na conexao: {:?}", e)),
  }
}

#[allow(dead_code)]
pub fn query(query: &'static str) -> Result<Vec<postgres::Row>, JsonValue> {
  match conexao::get_conn() {
    Ok(mut conn) => match conn.query(query, &[]) {
      Ok(r) => Ok(r),
      Err(e) => Err(json!(e.to_string())),
    },
    Err(e) => Err(json!(e.to_string())),
  }
}

#[allow(dead_code)]
pub fn get_one(query: &'static str, id: i32) -> Result<Vec<postgres::Row>, JsonValue> {
  match conexao::get_conn() {
    Ok(mut conn) => match conn.query(query, &[&id]) {
      Ok(r) => Ok(r),
      Err(e) => Err(json!(e.to_string())),
    },
    Err(e) => Err(json!(e.to_string())),
  }
}
